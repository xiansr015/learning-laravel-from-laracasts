@extends('layouts.master')

@section('content')
<div class="col-md-8 blog-main">
    <h1>{{ $post->title}}</h1>
    {{ $post->body }}
    <hr>
    Updated at {{ $post->updated_at }}
    <hr>

    <!-- Tags -->
    @if (count($post->tags))

        <ul>

            @foreach($post->tags as $tag)

                <li>

                    <a href="/posts/tags/{{ $tag->name }}">

                        {{ $tag->name }}

                    </a>
                    
                </li>

            @endforeach
            
        </ul>

    @endif
    <!-- Tags -->
    
    <!--Comments-->
    <div class="comments">
        <ul class="list-group">
        @foreach ($post->comments as $comment)
            <li class="list-group-item">
                {{ $comment->body }}
                <strong> &emsp; {{ $comment->created_at->diffForHumans() }} </strong>
            </li>
        @endforeach
    </div><!--Comments-->

    <hr>
    <!-- let them add a comment -->
    <div class="card">
        <div class="card-block">
            <form method="POST" action="/posts/{{ $post->id }}/comments">
            {{ csrf_field() }}
                <div class="form-group">
                    <textarea id="body" name="body" class="form-control"  placeholder="Write down your comments here." required></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Add comment</button>
                </div>
            </form>
            @include('layouts.errors')
        </div>
    </div>
    
    <!--nav Older/Newer-->
    <nav class="blog-pagination">
    <a class="btn btn-outline-primary" href="#">Older</a>
    <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
    </nav><!--nav Older/Newer-->
</div>
@endsection