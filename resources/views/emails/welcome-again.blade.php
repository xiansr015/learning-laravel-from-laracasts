@component('mail::message')
# Introduction

The body of your message.

- one
- two
- three

@component('mail::button', ['url' => 'Http://127.0.0.1/'])
View our page.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
