<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\RegistrationForm;

class RegisterationController extends Controller
{
    public function create(){
        return view('registration.create');
    }

    public function store(RegistrationForm $form){

        $form->persist();

        session()->flash('message', 'Thank so much for signing up!');

        //Redirect to the home page
        return redirect()->home();

    }
}
