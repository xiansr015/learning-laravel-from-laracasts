<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    
    public function __construct(){

        $this->middleware('guest', ['except' => 'destory']);

    }

    public function create(){
        return view('sessions.create');
    }

    public function destory(){
        auth()->logout();
        return redirect()->home();        
    }

    public function store(){
        //Attempt to authenticate the user.
        if(! auth()->attempt(request(['email', 'password']))){
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);
        }
        //if so, sign them in.
        return redirect()->home();

    }

}
